package com.example.homework201021;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
public class UserController {
    private List<User> users = new ArrayList<>();

    //добавляем пользователя в список
    /* curl -X POST http://localhost:8080/users
            -H 'Content-Type: application/json'
            -d '{"name":"Vladimir","age":"18"}' */
    @PostMapping("users")
    public void addUser(@RequestBody User user) {
        users.add(user);
    }

    //удаляем пользователя по индексу
    /* curl -X DELETE http://localhost:8080/users/0
            -H 'Content-Type: application/json'*/
    @DeleteMapping("users/{index}")
    public void deleteUser(@PathVariable("index") Integer index) {
        users.remove((int) index);
    }

    //выдать пользователя по индексу
    /* curl -X GET http://localhost:8080/users/0
            -H 'Content-Type: application/json'*/
    @GetMapping("users/{index}")
    public User getUser(@PathVariable("index") Integer index) {
        return users.get(index);
    }

    //выводим список пользователей
    /* curl -X GET http://localhost:8080/users
            -H 'Content-Type: application/json'*/
    @GetMapping("users")
    public List<User> getUsers() {
        return users;
    }

    //обновляем возраст пользователя по индексу
    /*curl -X PUT http://localhost:8080/userList/0
           -H 'Content-Type: application/json'
           -d '{"age":"10"}'*/
    @PutMapping("userList/{index}")
    public void updateAgeOfUser(
            @PathVariable("index") Integer index,
            @RequestBody Integer age) {
        users.get(index).setAge(age);
    }
}
