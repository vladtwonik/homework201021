package com.example.homework201021;


import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
public class KApiController {
    private List<String> messages = new ArrayList<>();

    //вывести список сообщений
    /* curl -X GET http://localhost:8080/messages
            -H 'Content-Type: application/json'*/
    @GetMapping("messages")
    public List<String> getMessages() {
        return messages;
    }

    //добавляем сообщение в список
    /* curl -X POST http://localhost:8080/messages
            -H 'Content-Type: application/json'
            -d 'example_of_message' */
    @PostMapping("messages")
    public void addMessage(@RequestBody String text) {
        messages.add(text);
    }

    //выдать сообщение по индексу
    /* curl -X GET http://localhost:8080/messages/{index}*/
    @GetMapping("messages/{index}")
    public String getMessage(@PathVariable("index") Integer index) {
        return messages.get(index);
    }

    //удаляем сообщение по индексу
    /* curl -X DELETE http://localhost:8080/messages/{index}
            -H 'Content-Type: application/json'*/
    @DeleteMapping("messages/{index}")
    public void deleteText(@PathVariable("index") Integer index) {
        messages.remove((int) index);
    }

    //обновляем сообщение по индексу
    /* curl -X PUT http://localhost:8080/messages/{index}
            -H 'Content-Type: application/json'
            -d "index":"index" */
    @PutMapping("messages/{index}")
    public void updateMessage(
            @PathVariable("index") Integer index,
            @RequestBody String message) {
        messages.remove((int) index);
        messages.add(index, message);
    }

    //возвращает индекс первого текста с подстрокой text
    /* curl -X GET http://localhost:8080/messages/search/example*/
    @GetMapping("messages/search/{text}")
    public Integer getMessage(@PathVariable("text") String text,
                              @RequestBody String message) {
        int i = messages.indexOf(text);
        return i;
    }

    //возвращает количество сообщений
    /* curl -X GET http://localhost:8080/messages/count*/
    @GetMapping("messages/count")
    public Integer getCount() {
        return messages.size();
    }

    //добавляет сообщение с порядковым номером index
    /* curl -X POST http://localhost:8080/messages/0/create
            -H 'Content-Type: application/json'
            -d "index":"0" */
    @PostMapping("messages/{index}/create")
    public void addMessageWithIndex(
            @PathVariable("index") Integer index,
            @RequestBody String text) {
        messages.add(index,text);
    }

    //удаляет все сообщения в которых есть подстрока text
    /* curl -X DELETE http://localhost:8080/messages/search/{text}
            -H 'Content-Type: application/json'*/
    @DeleteMapping("messages/search/{text}")
    public void deleteTextInAll(@PathVariable("text") String text) {
        for (int i = 0; i < messages.size(); i++) {
            if (messages.get(i).contains(text)) {
                messages.remove(i);
            }
        }
    }
}
