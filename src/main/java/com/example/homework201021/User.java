package com.example.homework201021;

public class User {
    private int age;
    private String name;

    public User(int age) {
        this.age = age;
    }

    public User(String name) {
        this.name = name;
    }

    public User(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
/*
Задание на оценку 5

Создать класс User, который хранит имя и возраст. Добавить в него конструкторы, геттеры и
сеттеры.
Написать контроллер, который делает следующее:
a. Добавлять пользователя в список
b. Удаляет пользователя по индексу
c. Выводить на экран пользователя по индексу
d. Выводить список пользователей
e. Обновляет возраст пользователя по индексу
Написать пример curl запроса для каждого сообщения.
в curl запросах вместо text/plain использовать application/json
*/